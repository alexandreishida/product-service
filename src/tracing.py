import config
import grpc_opentracing
from jaeger_client import Config

import logging
logging.getLogger("").handlers = []
logging.basicConfig(format="%(message)s", level=logging.DEBUG)

def init_jaeger_tracer(service_name):
    jaeger_config = Config(
        config={
            "sampler": {
                "type": "const",
                "param": 1,
            },
            "local_agent": {
                "reporting_host": config.JAEGER_HOST,
                "reporting_port": config.JAEGER_PORT
            },
            "logging": True,
            "reporter_batch_size": 1
        },
        service_name=service_name
    )

    return jaeger_config.initialize_tracer()

tracer = init_jaeger_tracer(config.TRACER_SERVICE_NAME)

grpc_client_interceptor = grpc_opentracing.open_tracing_client_interceptor(tracer)
