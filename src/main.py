from server import mongo, app

mongo.products.delete_many({})
for i in range(1, 3):
    mongo.products.insert_one({
            "_id": "product-{}".format(i),
            "price_in_cents": i,
            "title": "Product {}".format(i),
            "description": "The Product {}.".format(i)
        })
