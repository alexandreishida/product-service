import os

MONGO_HOST = os.environ.get("MONGO_HOST", "172.17.0.1")
MONGO_PORT = int(os.environ.get("MONGO_PORT", 27017))
MONGO_DBNAME = os.environ.get("MONGO_DBNAME", "store-dev")

DISCOUNT_SERVICE_ADDRESS = os.environ.get("DISCOUNT_SERVICE_ADDRESS", "172.17.0.1:50051")

JAEGER_HOST = os.environ.get("JAEGER_HOST", "172.17.0.1")
JAEGER_PORT = os.environ.get("JAEGER_PORT", 6831)

TRACER_SERVICE_NAME = "product-service"
