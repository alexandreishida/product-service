# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

import discount_grpc_pb2 as discount__grpc__pb2


class DiscountServiceStub(object):
  # missing associated documentation comment in .proto file
  pass

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.GetDiscount = channel.unary_unary(
        '/discount_grpc.DiscountService/GetDiscount',
        request_serializer=discount__grpc__pb2.DiscountRequest.SerializeToString,
        response_deserializer=discount__grpc__pb2.DiscountResponse.FromString,
        )


class DiscountServiceServicer(object):
  # missing associated documentation comment in .proto file
  pass

  def GetDiscount(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_DiscountServiceServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'GetDiscount': grpc.unary_unary_rpc_method_handler(
          servicer.GetDiscount,
          request_deserializer=discount__grpc__pb2.DiscountRequest.FromString,
          response_serializer=discount__grpc__pb2.DiscountResponse.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'discount_grpc.DiscountService', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
