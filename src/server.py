from flask import Flask, request, jsonify
from pymongo import MongoClient

import config
from grpc_clients import DiscountServiceClient
from services import ProductService
from tracing import tracer
from utils import ApiError


mongo = MongoClient(config.MONGO_HOST, config.MONGO_PORT).get_database(config.MONGO_DBNAME)

discount_service_address = config.DISCOUNT_SERVICE_ADDRESS
discount_service = DiscountServiceClient(discount_service_address)

def get_product_service(user_id):
    return ProductService(user_id, mongo, discount_service)


# main

app = Flask(__name__)

@app.errorhandler(ApiError)
def handle_api_error(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

@app.route("/product")
def get_products():
    with tracer.start_active_span("get_products") as scope:
        user_id = request.headers.get("X-USER-ID")

        if user_id == "":
            raise ApiError("invalid header X-USER-ID")

        product_service = get_product_service(user_id)

        products = product_service.get_products()

        return jsonify(products)
