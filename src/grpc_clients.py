import discount_grpc_pb2
import discount_grpc_pb2_grpc
import grpc
from grpc_opentracing.grpcext import intercept_channel
from tracing import tracer, grpc_client_interceptor

class DiscountServiceClient:
    def __init__(self, server_address):
        self.server_address = server_address

    def get_discount(self, user_id, product_id):
        with tracer.start_active_span("grpc_clients.DiscountServiceClient.get_discount") as scope:
            discount = 0.0

            try:
                with grpc.insecure_channel(self.server_address) as channel:
                    channel = intercept_channel(channel, grpc_client_interceptor)
                    stub = discount_grpc_pb2_grpc.DiscountServiceStub(channel)

                    if user_id:
                        user_id = user_id.encode("utf8")

                    if product_id:
                        product_id = product_id.encode("utf8")

                    request = discount_grpc_pb2.DiscountRequest(
                                                user_id=user_id,
                                                product_id=product_id)
                    response = stub.GetDiscount(request)
                    discount = response.discount
            except grpc.RpcError as error:
                if error.code() == grpc.StatusCode.UNAVAILABLE:
                    pass
                else:
                    raise error

            return discount
