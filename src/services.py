from tracing import tracer

class ProductService:

    def __init__(self, user_id, mongo, discount_service):
        self.user_id = user_id
        self.mongo = mongo
        self.discount_service = discount_service

    def get_products(self):
        with tracer.start_active_span("services.ProductService.get_products") as scope:
            products = self.extract()
            return self.transform(products)

    def extract(self):
        with tracer.start_active_span("services.ProductService.extract") as scope:
            cursor = self.mongo.products.find().limit(10)
            return list(cursor)

    def transform(self, products):
        with tracer.start_active_span("services.ProductService.transform") as scope:
            for product in products:
                product_id = str(product["_id"])
                discount = self.get_discount(product_id)
                if discount > 0:
                    value = product["price_in_cents"] * discount / 100
                    product["discount"] = {
                        "pct": discount,
                        "value_in_cents": value
                    }
            return products

    def get_discount(self, product_id):
        with tracer.start_active_span("services.ProductService.get_discount") as scope:
            return self.discount_service.get_discount(self.user_id, product_id)
