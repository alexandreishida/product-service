import pytest
from unittest.mock import MagicMock

import server
from server import app


product_service = MagicMock()
product_service.get_products.return_value = ["foo", "bar", "baz"]


@pytest.fixture
def client(mocker):
    mocker.patch("server.get_product_service", return_value=product_service)

    yield app.test_client()

class TestGetProducts:

    def test_return_with_user_id_header(self, client):
        response = client.get("/product", headers={"X-USER-ID": "user-uid"})

        assert response.status_code == 200
        assert response.get_json() == ["foo", "bar", "baz"]

    def test_return_without_user_id_header(self, client):
        response = client.get("/product")

        assert response.status_code == 200
        assert response.get_json() == ["foo", "bar", "baz"]

    # def test_return_with_empty_user_id_header(self, client):
    #     response = client.get("/product", headers={"X-USER-ID": ""})

    #     assert response.status_code == 400
    #     assert response.get_json() == {"message": "invalid header X-USER-ID"}
