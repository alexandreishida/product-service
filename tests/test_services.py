from pymongo import MongoClient
import pytest
from unittest.mock import MagicMock

import config
from grpc_clients import DiscountServiceClient
from services import ProductService


mongo = MongoClient(config.MONGO_HOST, config.MONGO_PORT).get_database("store-test")

discount_service = MagicMock()
discount_service.get_discount.return_value = 10.0


class TestProductService:

    @pytest.fixture(scope="class")
    def service(self):
        for i in range(1, 3):
            mongo.products.insert_one({
                    "_id": "product-{}".format(i),
                    "price_in_cents": i,
                    "title": "Product {}".format(i),
                    "description": "The Product {}.".format(i)
                })

        yield ProductService("user-uid", mongo, discount_service)

        mongo.products.delete_many({})

    def test_get_products(self, service):
        assert service.get_products() == [
            {
                "_id": "product-1",
                "price_in_cents": 1,
                "title": "Product 1",
                "description": "The Product 1.",
                "discount": {
                    "pct": 10.0,
                    "value_in_cents": 0.1
                }
            }, {
                "_id": "product-2",
                "price_in_cents": 2,
                "title": "Product 2",
                "description": "The Product 2.",
                "discount": {
                    "pct": 10.0,
                    "value_in_cents": 0.2
                }
            }
        ]

    def test_extract(self, service):
        assert service.extract() == [
            {
                "_id": "product-1",
                "price_in_cents": 1,
                "title": "Product 1",
                "description": "The Product 1."
            }, {
                "_id": "product-2",
                "price_in_cents": 2,
                "title": "Product 2",
                "description": "The Product 2."
            }
        ]

    def test_transform(self, service):
        product = {
            "_id": "product-uid",
            "price_in_cents": 9.5,
            "title": "Coca Cola 2L",
            "description": "Refrigerante saudável, na medida certa."
        }
        
        assert service.transform([product]) == [{
            **product,
            "discount": {
                "pct": 10.0,
                "value_in_cents": 0.95
            }
        }]

    def test_get_discount(self, service):
        assert service.get_discount("product-uid") == 10.0
