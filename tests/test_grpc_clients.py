from concurrent import futures
import grpc
import pytest

import discount_grpc_pb2
import discount_grpc_pb2_grpc
from grpc_clients import DiscountServiceClient


class DiscountServiceServicer(discount_grpc_pb2_grpc.DiscountServiceServicer):
    def GetDiscount(self, request, context):
        return discount_grpc_pb2.DiscountResponse(discount=0.666)


class TestDiscountServiceClient:

    @pytest.fixture(scope="class")
    def server_address(self):
        server = grpc.server(futures.ThreadPoolExecutor())
        discount_grpc_pb2_grpc.add_DiscountServiceServicer_to_server(DiscountServiceServicer(), server)
        port = server.add_insecure_port("[::]:0")
        server.start()

        yield "localhost:{}".format(port)

        server.stop(0)

    def test_success(self, server_address):
        discount = DiscountServiceClient(server_address).get_discount("user-uid", "product-uid")

        assert discount == 0.666

    def test_unavailability(self):
        invalid_address = "invalid-ip:invalid-port"
        discount = DiscountServiceClient(invalid_address).get_discount("user-uid", "product-uid")

        assert discount == 0.0
