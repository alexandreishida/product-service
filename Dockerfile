FROM python:3.6-slim

ADD . /app
WORKDIR /app

RUN pip install -r requirements.txt

ENV PYTHONPATH=src/
ENV FLASK_APP=main.py
CMD ["flask", "run", "--host=0.0.0.0"]

EXPOSE 5000
