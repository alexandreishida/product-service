# Product Service

## The challenge

Exposes an HTTP route such that GET /product returns a json with a list of products.

This route should optionally receive via X-USER-ID header a user id.

In order to obtain the personalized discount, this service must use **Discount Service**.

If **Discount Service** returns an error, the list of products still has to be returned, but with this product that gave error without discount.

If the **Discount Service** is down, the **Product Service** has to continue working and returning the list normally, just will not apply the discounts.

Restrictions:
- The services of this test should be written using different languages.
- The services of this test must communicate via `gRPC` (https://grpc.io/).
- Use docker to provision services.
- For ease of use, services can use a shared database.

## The result

### Requirements
- Python 3.6
- Pipenv
- Mongo
```bash
$ docker run --rm -d -p 27017:27017 mongo:4
```
- Jaeger
```bash
$ docker run --rm -p 6831:6831/udp -p 6832:6832/udp -p 16686:16686 jaegertracing/all-in-one:1.12 --log-level=debug
```

### Installation
```bash
$ pipenv install --dev
$ pipenv shell
```

### Test
```bash
$ PYTHONPATH=src/ pytest -v tests
======================================= test session starts ========================================
platform linux -- Python 3.6.8, pytest-4.5.0, py-1.8.0, pluggy-0.11.0 -- /usr/local/bin/python
cachedir: .pytest_cache
rootdir: /app
plugins: mock-1.10.4
collected 8 items                                                                                  

tests/test_grpc_clients.py::TestDiscountServiceClient::test_success PASSED                   [ 12%]
tests/test_grpc_clients.py::TestDiscountServiceClient::test_unavailability PASSED            [ 25%]
tests/test_server.py::TestGetProducts::test_return_with_user_id_header PASSED                [ 37%]
tests/test_server.py::TestGetProducts::test_return_without_user_id_header PASSED             [ 50%]
tests/test_services.py::TestProductService::test_get_products PASSED                         [ 62%]
tests/test_services.py::TestProductService::test_extract PASSED                              [ 75%]
tests/test_services.py::TestProductService::test_transform PASSED                            [ 87%]
tests/test_services.py::TestProductService::test_get_discount PASSED                         [100%]

===================================== 8 passed in 0.50 seconds =====================================
```

### Run
```bash
$ PYTHONPATH=src/ FLASK_APP=main.py flask run
```

### CI
- https://gitlab.com/alexandreishida/product-service/pipelines

### Docker run
```bash
$ docker login registry.gitlab.com
$ docker run --rm -it -p 5000:5000 -e DISCOUNT_SERVICE_ADDRESS=172.17.0.1:50051 registry.gitlab.com/alexandreishida/product-service:0.2
```

Some options can be set with `ENV` variables.

| ENV                      | Default          |
| ------------------------ | ---------------- |
| MONGO_HOST               | 172.17.0.1       |
| MONGO_PORT               | 27017            |
| MONGO_DBNAME             | store-dev        |
| DISCOUNT_SERVICE_ADDRESS | 172.17.0.1:50051 |
| JAEGER_HOST              | 172.17.0.1       |
| JAEGER_PORT              | 6831             |

You can use `curl` in order to test.
```bash
$ curl -H "X-USER-ID: user-3" http://localhost:5000/product
[{"_id":"product-1","description":"The Product 1.","discount":{"pct":5.0,"value_in_cents":0.05},"price_in_cents":1,"title":"Product 1"},{"_id":"product-2","description":"The Product 2.","discount":{"pct":5.0,"value_in_cents":0.1},"price_in_cents":2,"title":"Product 2"}]
```

### Instrumentation (Opentracing + Jaeger)
Access the Jaeger UI (http://localhost:16686).

![alt text](https://gitlab.com/alexandreishida/product-service/raw/master/screenshot-opentracing-jaeger.png "Opentracing + Jaeger")

### To Do
- Improve tracing (tags)
- Improve logging
- Secure GRPC
- Fix Dockerfile to use a production WSGI server
